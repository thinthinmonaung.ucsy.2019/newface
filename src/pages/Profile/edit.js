import React, { Component } from "react";
import { Input } from 'antd';
import { Button } from 'antd';//button
import { Row, Col,Card } from 'antd';
import PageHeaderWrapper from '../../components/PageHeaderWrapper'
import history from '../../router/history'
import { getUserInfo } from '../../utils'
import api from '../../apis'

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true
        }
    }

    async componentWillMount() {

        const user = getUserInfo();
        const response = await api.get('/account/user/' + user.id);
        console.log(response);
        this.setState({ data: response.data.data })

    }
    btnEdit = () => {
        history.push("proedit");

    }

    render() {

        const { data } = this.state;
        console.log(data);

        return (
            <div>
                <PageHeaderWrapper />
                <Card style={{margin:'3%'}}>
                <div style={{margin:'3%'}}>
                    <Row>
                        <Col span={8}>
                            <h3>Name</h3>
                            {/* <p>{data.empname}</p> */}
                            <Input value={data.empname} style={{width:"100px"}}/>
                        </Col>
                        <Col span={8}>
                            <h3>NRIC</h3>
                            <p>{data.empnric}</p>
                        </Col>
                        <Col span={8}>
                            <h3>Date Of Birth</h3>
                            <p>{data.empdob}</p>
        </Col>
                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Position</h3>
                            <p>{data.posname}</p>
        </Col>
                        <Col span={8}>
                            <h3>Department</h3>
                            <p>{data.depname}</p>
        </Col>
                        <Col span={8}>
                            <h3>Employee Code</h3>
                            <p>{data.ecode}</p>
        </Col>
                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Father</h3>
                            <p>{data.empfather}</p>
        </Col>
                        <Col span={8}>
                            <h3>Mother</h3>
                            <p>{data.empmother}</p>
        </Col>
                        <Col span={8}>
                            <h3>Phone No</h3>
                            <Input value={data.phone_number} style={{width:"150px"}}/>
        </Col>
                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Parment</h3>
                            <p>{data.empparmanent}</p>
        </Col>
                        <Col span={8}>
                            <h3>Tamporery</h3>
                            <p>{data.emptemporary}</p>
        </Col>

                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Education</h3>
                            <p>{data.empedu}</p>
        </Col>
                        <Col span={8}>
                            <h3>Social Media</h3>
                            <p>{data.empsocial}</p>
        </Col>

                    </Row><br />
                    <Row><br />
                        <Col span={8}>
                            <Button onClick={this.btnEdit} style={{ color: "white", background: "green", paddingRight: "28px", paddingLeft: "28px" }}>Edit</Button>
                        </Col>
                    </Row>
                </div>
                </Card>
            </div>
        )
    }
}
export default Form