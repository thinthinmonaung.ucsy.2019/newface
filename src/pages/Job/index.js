import React from 'react';
//component
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import ScrollTable from './CustomScrollTable';
import Can from '../../../src/utils/Can';
import Forbidden from '../Forbidden';
import { fetchSchedules, fetchSchedule, putSchedule, postSchedule, deleteSchedule } from '../../actions/Schedule';
import { connect } from "react-redux";
import { getUserInfo } from '../../utils'
import api from '../../apis'

const uuidv4 = require('uuid/v4');


class Schedule extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataSource1: [],
    }
  }

  componentDidMount() {
    this.getAllSchedule();
    this.getData();
  }

  async getData() {
    const user = getUserInfo();
    const response = await api.get('/account/user/' + user.id);
    const response1 = await api.get(`schedules/${response.data.data.employee_id}`);
    this.setState({ data: response.data.data });
    console.log("Account", response.data.data.employee_id)
  }

  getAllSchedule() {
    this.props.fetchSchedules()
  }
  getSchedule(id) {
    this.props.fetchSchedule(id);
  }

  //update Schedule
  editSchedule = (data, id) => {
    this.props.putSchedule(data, id);
  }

  // to create new Schedule
  createNewSchedule = (data) => {
    data.created_by = "admin";
    data.updated_by = '';
    this.props.postSchedule(data);
  }

  // to delete Schedule
  deleteSchedule = (id) => {
    this.props.deleteSchedule(id);
  }


  render() {
    const { selectedRowKeys } = this.state;
    var { data, dataSource1 } = this.state;
    var dataSource = this.props.schedules
    console.log("Data", data)

    console.log("A", data.job_code)
    const columns = [
      {
        title: 'Job Code',
        dataIndex: 'job_code',
        key: 'job_code',
        align: 'center',
        width: 150,
        sorter: (a, b) => a.job_code.length - b.job_code.length,
        sortDirections: ['ascend', 'descend'],
        fixed: 'left'

      },
      {
        title: 'Complain Number',
        dataIndex: 'complain_number',
        key: '1',
        align: 'center',
        sorter: (a, b) => a.complain_number.length - b.complain_number.length,
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Service Man',
        dataIndex: 'employee_name',
        key: '2',
        align: 'center',
        sorter: (a, b) => a.employee_name.length - b.employee_name.length,
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Start Date',
        dataIndex: 'sdate',
        key: '3',
        align: 'center',
      },
      {
        title: 'End Date',
        dataIndex: 'edate',
        key: '4',
        align: 'center',
      },
      {
        title: 'Status',
        dataIndex: 'job_status',
        align: 'center',
        key: 'job_status',
        width: 110,
        fixed: 'right'
      },

    ];
    const perform = {
      create: "position:create",
      edit: "position:edit"
    }
    const newData = {
      title: "",
      description: ""
    }
    return (
      <div>
        <Can
          role="Admin"
          perform="position:list"
          no={() => {
            return <Forbidden />
          }}
        >
          <PageHeaderWrapper />
          <ScrollTable
            dataSource={dataSource}
            columns={columns}
            title="Job"
            role="Admin"
            perform={perform}
            newData={newData}
            getData={this.getAllSchedule}
            getdata={(id) => this.getSchedule(id)}
            editData={(data, id) => this.editSchedule(data, id)}
            createNewData={(data) => this.createNewSchedule(data)}
            deleteData={(id) => this.deleteSchedule(id)}
          />
        </Can>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    schedules: state.schedule.list,
  };
}
export default connect(
  mapStateToProps,
  { fetchSchedules, fetchSchedule, putSchedule, postSchedule, deleteSchedule }
)(Schedule);
