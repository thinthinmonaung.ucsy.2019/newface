import React from 'react';
import { Table, Popconfirm, Icon, Button, Col, Row, Input, Select, Breadcrumb } from 'antd';
import history from '../../router/history'
import PageHeaderWrapper from '../../components/PageHeaderWrapper'
//component
import EditableTable from '../../components/Report/CustomTable';

import { getUserInfo, getUserToken } from '../../utils';
import { fetchReport, putReport, postReport, deleteReport } from '../../actions/Report';
import { connect } from "react-redux";
const uuidv4 = require('uuid/v4');


class Report extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    this.getAllReport();
  }

  getAllReport() {
    this.props.fetchReport()
  }

  //update Report
  editReport = (data, id) => {
    this.props.putReport(data, id);
  }

  // to create new Report
  createNewReport = (data) => {
    let userInfo = getUserInfo();
    data.created_by = "admin";
    data.updated_by = '';
    this.props.postReport(data);
  }

  // to delete Report
  deleteReport = (id) => {
    this.props.deleteReport(id);
  }

  btnCreate = () => {
    history.push('/report/create')
  }

  render() {
    var columns = [
      {
        title: 'Job Code',
        width: 200,
        dataIndex: 'job_code',
        fixed: 'left',
        align: 'center',
        sorter: (a, b) => a.job_code.length - b.job_code.length,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'FUP No',
        dataIndex: 'fup_number',
        key: '1',
        align: 'center',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.fup_number - b.fup_number,
      },
      {
        title: 'Working Hour',
        dataIndex: 'wkh',
        key: '2',
        align: 'center',
        sorter: (a, b) => a.wkh.length - b.wkh.length,
        sortDirections: ['descend', 'ascend'],
      },

      {
        title: 'Warranty Description',
        dataIndex: 'wd',
        key: '3',
        align: 'center',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.wd - b.wd,
      },
      {
        title: 'Remark',
        dataIndex: 'remark',
        key: '3',
        align: 'center',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.remark - b.remark,
      },
      {
        title: 'Date',
        dataIndex: 'date',
        key: '4',
        align: 'center',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.date - b.date,
      },


    ];
    const perform = {
      create: "report:create",
      edit: "report:edit"
    }
    const newData = {
      title: "",
      description: ""
    }

    let data = this.props.report;
    data.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
      console.log("KEY: " + d.key);
    })

    return (
      <div >
        <PageHeaderWrapper />
        <Row style={{ marginBottom: '30px', marginTop: '30px' }}>
          <Button type="primary" style={{ backgroundColor: '#082C13', height: '50px' }} onClick={this.btnCreate} >Create New Report</Button>
        </Row>
        <EditableTable
          dataSource={data}
          columns={columns}
          role="Admin"
          title="Daily Report"
          size="middle"
          perform={perform}
          newData={newData}
          editData={(data, id) => this.editReport(data, id)}
          createNewData={(data) => this.createNewReport(data)}
          deleteData={(id) => this.deleteReport(id)}
        />

      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    report: state.report.list,
  };
}
export default connect(
  mapStateToProps,
  { fetchReport, putReport, postReport, deleteReport }
)(Report);
