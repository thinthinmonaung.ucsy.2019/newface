import React from 'react'
import { Row, Col } from 'antd';
import { Card } from 'antd';
import { Divider,Breadcrumb } from 'antd';
import SelectTable from './SelectTable';
import { Link } from 'react-router-dom';
import api from 'apis';
import { fetchComplain } from '../../../actions/Complain'
import { fetchEmployee } from '../../../actions/Employee';

import { connect } from "react-redux";
const uuidv4 = require('uuid/v4');
const moment = require('moment');


class AssignSchedule extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            data: '',
            machine_id : '',
            department_id : ''
        }
    }

    componentDidMount() {
        this.getData();
        this.props.fetchComplain()
        this.props.fetchEmployee()
    }

    async getData() {
        const response = await api.get(`complains/${this.state.id}`);
        if (response && response.status == 200) {
            this.setState({ data: response.data.data, machine_id: response.data.data.machine_id, department_id: response.data.data.department_id });
        }

    }

    render() {
        const {data, machine_id, department_id} = this.state;
        
        const dataSource = this.props.complain;
        
        dataSource.map(d => {
            let uuid = uuidv4();
            d.key = uuid;
        })
        let dataService = this.props.employee;
        dataService.map(d => {
            let uuid = uuidv4();
            d.key = uuid;
        })

        const { working_hour, model_number, complain_number,
            fup_number,
            warranty_year,
            warranty_description,
            customer_phone,
            customer_name,
            name,
            distance,
            job_title,
            date,
            amount,
            complain_job_title,
            complain_description, location } = this.state.data;

            const columns = [
                {
                    title: 'Model No',
                    dataIndex: 'model_number',
                    width: '200px',
                    fixed:'left'
    
                },
                {
                    title: 'FUP No',
                    dataIndex: 'fup_number',
                    width: '20%',
                },
                {
                    title: 'Complain No',
                    dataIndex: 'complain_number',
                    width: '20%',
    
                },
                {
                    title: 'Date',
                    dataIndex: 'date',
                    width: '20%',
    
                },
                {
                    title: 'Status',
                    dataIndex: 'complain_status',
                    width: '200px',
                    fixed:'right'
    
                },
                {
                    title: 'Action',
                    align: 'center',
                    width: '200px',
                    fixed:'right',
                    render: record => (
                      <>
                         <Link style={{ color: 'green',marginRight:'0.5em' }} to={"/assigntoschedule/machineview/" + record.id}>View</Link>
                      </>
                    )
                  }
            ];
    

        return (
            <div>
                <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold', marginBottom: '20px' }}>
                    <Breadcrumb.Item>
                        <a href="/">Configuration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/assign to schedule">Assign to Schedule</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="" style={{ color: 'green' }}>Accept Schedule</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Card title="Complain Information" style={{ width: '95%', marginRight: '2.5%',marginTop:'5%', marginLeft: '2.5%', borderRadius: '14px' }}>
                    <Row>
                        <Col span={5} offset={1}>Complain Number : </Col>
                        <Col span={4}>{complain_number}</Col>
                        <Col span={6} offset={4}>Model Number : </Col>
                        <Col span={4}>{model_number}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Working hour : </Col>
                        <Col span={4}>{working_hour}</Col>
                        <Col span={6} offset={4}>Warranty Description : </Col>
                        <Col span={4}>{warranty_description}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>FUP Number : </Col>
                        <Col span={4}>{fup_number}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Warranty Year : </Col>
                        <Col span={4}>{warranty_year}</Col>
                        <Col span={6} offset={4}>Customer Ph No : </Col>
                        <Col span={4}>{customer_phone}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Customer Name : </Col>
                        <Col span={4}>{customer_name}</Col>
                        <Col span={6} offset={4}>Location : </Col>
                        <Col span={4}>{location}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Distance : </Col>
                        <Col span={4}>{distance}</Col>
                        <Col span={6} offset={4}>Department : </Col>
                        <Col span={4}>{name}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Date:</Col>
                        <Col span={4}>{moment(date).format('YYYY-MM-DD')}</Col>
                        <Col span={6} offset={4}>Job Title:</Col>
                        <Col span={4}>{job_title}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Amount : </Col>
                        <Col span={4}>{amount}</Col>
                        <Col span={6} offset={4}>Description : </Col>
                        <Col span={4}>{complain_description}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Complain Job Title : </Col>
                        <Col span={4}>{complain_job_title}</Col>
                    </Row>

                </Card>
                <div style={{ width: '95%', marginRight: '2.5%',marginTop:'5%', marginLeft: '2.5%', borderRadius: '14px' }}>
                </div>

                <div style={{ width: '95%', marginRight: '2.5%',marginTop:'5%', marginLeft: '2.5%', borderRadius: '14px' }}>
                    <p style={{marginBottom:'2.5%'}}>You can assign your schedule information.</p>
                    {
                        data != '' ? <SelectTable
                        complain_id={this.state.id}
                        machine_id={machine_id}
                        department_id={department_id}
                        dataSource={dataService}
                        /> : ''
                    }
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedIn: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        complain: state.complain.list,
        employee: state.employee.list
    };
}
export default connect(
    mapStateToProps,
    { fetchComplain, fetchEmployee }
)(AssignSchedule);