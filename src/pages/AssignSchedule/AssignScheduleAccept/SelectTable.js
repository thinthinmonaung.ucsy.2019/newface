import { Table } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom'
import { DatePicker, Divider,Card } from 'antd';
import { Radio, Button } from 'antd';
import { Row, Col, Form, Input, Select } from 'antd';
import api from 'apis';
import history from '../../../router/history'
import { noti } from 'utils/index';
//import moment from 'moment';
const uuidv4 = require('uuid/v4');
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Option } = Select;


const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    fixed: 'left',
    align: 'center',
    width: 150
  },
  {
    title: 'Position',
    key: '1',
    align: 'center',
    dataIndex: 'posname'
  },
  {
    title: 'Phone No',
    dataIndex: 'phone',
    key: '2',
    align: 'center',
  },
  {
    title: 'Address',
    dataIndex: 'parmanent_address',
    key: '3',
    align: 'center',
  },
  {
    title: 'NRIC',
    dataIndex: 'nric',
    key: '4',
    align: 'center',
  },
  {
    title: 'Employee Code',
    dataIndex: 'code',
    key: '5',
    align: 'center',
  },

  {
    title: 'Action',
    key: 'operation',
    align: 'center',
    width: 200,
    fixed: 'right',
    render: record => (
      <>
        <Link style={{ color: 'green', marginRight: '0.5em' }} to={"/employees/detail/" + record.id}>View</Link>
      </>
    )
  },
];


class As extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      complain_id: props.complain_id,
      machine_id: props.machine_id,
      department_id: props.department_id,
      count: props.count,
      currentPagination: 1,
      customPagesize: 5,
      selectedRowKeys: [],
      selectedid: [],
      employee_id: [],

    };

  }
  handleSubmit = e => {
    const selectedRowKeys = this.state.selectedRowKeys;
    const selectedid = this.state.selectedid;
    const employee_id = this.state.employee_id;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
      if (!err) {
        const rangev = fieldsValue['range'];
        const emp_array = selectedid
        const emp_id = employee_id

        const emp_string = emp_array.join();
        const emp_iid = emp_id.join()

        const values = {
          complain_id: this.state.complain_id,
          machine_id: this.state.machine_id,
          department_id: this.state.department_id,
          job_code: fieldsValue.job_code,
          job_description: fieldsValue.job_description,
          job_title: fieldsValue.job_title,
          s_amount: fieldsValue.s_amount,
          job_status: fieldsValue.job_status,
          inspection: fieldsValue.inspection,
          watching_list: fieldsValue.watching_list,
          service_charge: fieldsValue.service_charge,
          sdate: rangev[0].format('YYYY/MM/DD'),
          edate: rangev[1].format('YYYY/MM/DD'),
          employee_name: emp_string,
          employee_id: emp_iid
        }


        console.log(values.machine_id, "wearone")
        const values1 = {
          complain_status: fieldsValue.job_status
        }
        api.post(`schedules`, values).then((result) => {
          if (result) {
            history.push('/schedules')
            noti('success', 'Successfully!', 'Schedules have been created successfully.')
          }
        }).then(() => {
          const data = {
            complain_status: 'Accepted'
          }
          api.put(`complains/${this.state.complain_id}`, data)
        })
      } else {
        noti('error', 'Unsuccessfully!', 'Fail to Create.')
      }
    });
  };


  async postData(v) {
    api.post('schedules', v);
  }

  onSelectChange = (selectedRowKeys, selectedRow) => {
    let did = selectedRow.map(r => r.name);
    let eid = selectedRow.map(r => r.id);
    this.setState({ selectedRowKeys, selectedid: did, employee_id: eid });
  };
  componentDidUpdate(prevProps) {
    if (this.props.dataSource !== prevProps.dataSource) {
      this.setState({
        loading: false,
        data: this.props.dataSource,
        count: this.props.count
      });
    }
  }

  btnCancel = () => {
    history.push('/assign to schedule')
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { data, selectedRowKeys } = this.state;

    const dateFormat = 'YYYY/MM/DD';
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,

      onSelection: this.onSelection,
    };

    const renderModel = (
      <Select style={{
        width: '80%'
      }} placeholder="Please select Job Status">
        {["Assign", "Ongoing", "Accept"].map(item => {
          return <Option value={item}>{item}</Option>
        })}
      </Select>
    )
    return (
      <div>
        <Form onSubmit={this.handleSubmit} >
          <Row>
            <Col span={9} offset={1}>
              <Form.Item label='Interval Date:'>

                {getFieldDecorator(['range'], {
                  rules: [{
                    type: 'array',
                    required: true,
                    message: 'Please input your range!'
                  }]
                })(<RangePicker
                  format={dateFormat} />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={9} offset={1}>
              <Form.Item label='Amount:'>

                {getFieldDecorator('s_amount', {
                  rules: [{
                    required: true,
                    message: 'Please input your amount!'
                  }]
                })(<Input addonAfter="Kyats" />)}
              </Form.Item>
            </Col>
            <Col span={9} offset={1}>
              <Form.Item label='Service Charge:'>

                {getFieldDecorator('service_charge', {
                  rules: [{
                    required: true,
                    message: 'Please input Service Charge!'
                  }]
                })(<Input addonAfter="Kyats" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={4} offset={1}>
              <Form.Item label='Inspection:'>

                {getFieldDecorator('inspection')(<Radio.Group>
                  <Radio value='Yes'>Yes</Radio>
                  <Radio value='No'>No</Radio>
                </Radio.Group>)}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label='Watching List:'>
                {getFieldDecorator('watching_list')(<Radio.Group>
                  <Radio value='Yes'>Yes</Radio>
                  <Radio value='No'>No</Radio>
                </Radio.Group>)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={9} offset={1}>
              <Form.Item label='Job Code:'>

                {getFieldDecorator('job_code', {
                  rules: [{
                    required: true,
                    message: 'Please input Job Code!'
                  }]
                })(<Input placeholder="Enter Code" />)}
              </Form.Item>
            </Col>
            <Col span={9} offset={1}>
              <Form.Item label='Job Status:'>

                {getFieldDecorator('job_status', {
                  rules: [{
                    required: true,
                    message: 'Please input Job Status!'
                  }]
                })(renderModel)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={9} offset={1}>
              <Form.Item label='Job Title:'>

                {getFieldDecorator('job_title', {
                  rules: [{
                    required: true,
                    message: 'Please input Job title!'
                  }]
                })(<Input placeholder="Enter Title" />)}
              </Form.Item>
            </Col>
            <Col span={9} offset={1}>
              <Form.Item label='Job Description:'>

                {getFieldDecorator('job_description', {
                  rules: [{
                    required: true,
                    message: 'Please input Job Description!'
                  }]
                })(<TextArea rows={3} placeholder='Enter Description' />)}
              </Form.Item>
            </Col>
          </Row>
          <Card>
            <Table
              key={data.key}
              rowSelection={rowSelection}
              dataSource={data}
              columns={columns}
              title={() => <h2>Service Man Information</h2>}
              rowClassName="editable-row"
              scroll={{ x: 2000 }}
            />
          </Card>
          <Button htmlType="submit" style={{ marginTop: '2.5%', backgroundColor: "green", color: "white", borderColor: "green" }}>Assign</Button>

          <Button style={{ marginTop: '2.5%', marginLeft: '2.5%', backgroundColor: "white", color: "black", borderColor: "gray" }} onClick={this.btnCancel}>Cancel</Button>
        </Form>
      </div>);
  }
}
const SelectTable = Form.create()(As);
export default SelectTable;