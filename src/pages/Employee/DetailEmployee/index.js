import React from 'react'
import { Breadcrumb, Form } from 'antd';
import { Divider } from 'antd';
import { Button, Col } from 'antd';
import { Avatar } from 'antd';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
import './index.css'
import history from '../../../router/history'
import api from 'apis';
import PageHeaderWrapper from '../../../components/PageHeaderWrapper'

const imgurl = "http://localhost:9991/"
class Employee extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            data: []
        }
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await api.get(`employees/${this.state.id}`);
        if (response && response.status == 200) {
            this.setState({ data: response.data.data })
        }
    }

    btnCancel = () => {
        history.push('/employees')
    }
    render() {
        const data = this.state.data;

        console.log();

        return (
            <div>
                <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold', marginBottom: '20px' }}>
                    <Breadcrumb.Item>
                        <a href="/">Configuration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/employees">Employee</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="" style={{ color: 'green' }}>View Employee</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Col span={24}>
                    <Avatar
                        style={{ marginLeft: '44%', border: '1px solid gray' }}
                        src={imgurl + data.image}
                        shape="circle" size={124}
                    />
                    <div style={{ marginLeft: '46%', marginBottom: '50px' }}>
                        <h3>
                            {data.name}
                        </h3>
                        <h3>
                            <p>Code : {data.code}</p>
                        </h3>
                    </div>
                </Col>

                <div style={{ marginLeft: '10%' }}>
                    <div>
                        <Col span={10}>
                            <h3>NIRC</h3>
                            <p>{data.nric}</p>
                        </Col>
                        <Col span={9}>
                            <h3>Position</h3>
                            <p>{data.posname}</p>
                        </Col>
                        <Col span={5} style={{ marginBottom: '40px' }}>
                            <h3>Department</h3>
                            <p>{data.depname}</p>
                        </Col>
                    </div>

                    <div>
                        <Col span={10}>
                            <h3>Date Of Birth</h3>
                            <p>{data.dob}</p>
                        </Col>

                        <Col span={9}>
                            <h3>Father</h3>
                            <p>{data.father_name}</p>
                        </Col>
                        <Col span={5} style={{ marginBottom: '40px' }}>
                            <h3>Mother</h3>
                            <p>{data.mother_name}</p>
                        </Col>
                    </div>

                    <div>
                        <Col span={10}>
                            <h3>Parment Address</h3>
                            <p>{data.parmanent_address}</p>
                        </Col>
                        <Col span={14} style={{ marginBottom: '40px' }}>
                            <h3>Phone Number</h3>
                            <p>{data.prefixphone+" "}{data.phone}</p>
                        </Col>
                    </div>

                    <div>
                        <Col span={24} style={{ marginBottom: '40px' }}>
                            <h3>Temporary Address</h3>
                            <p>{data.temporary_address}</p>
                        </Col>
                    </div>

                    <div>
                        <Col span={24} style={{ marginBottom: '40px' }}>
                            <h3>Education</h3>
                            <p>{data.education}</p>
                        </Col>
                    </div>

                    <div>
                        <Col span={24} style={{ marginBottom: '40px' }}>
                            <h3>Social Media</h3>
                            <p>{data.social_media_link}</p>
                        </Col>
                    </div>
                    <div>
                        <Form.Item wrapperCol={{ span: 24 }}>
                            <Link to={`/employees/edit/${this.state.id}`}><Button style={{ color: 'white', backgroundColor: 'green', padding: '0px 27px', borderColor: 'green' }} type="edit">Edit</Button></Link>
                            <Button type="primary" style={{ marginLeft: '2%', backgroundColor: "white", color: "black", borderColor: "gray" }} onClick={this.btnCancel} >
                                Cancel
                    </Button>
                        </Form.Item>
                    </div>
                </div>
            </div>
        )
    }
}
export default Employee;