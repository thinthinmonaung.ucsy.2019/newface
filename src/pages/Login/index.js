//react
import React from 'react'
import QueueAnim from 'rc-queue-anim'
import history from '../../router/history'
import config from '../../utils/config'
//redux
import { connect } from 'react-redux'
import { signIn, currentUser } from '../../actions/Auth'
//ant
import { Button, Icon, Form, Input, Layout } from 'antd'
//image
import logo from '../../logo.png'
//css
import api from '../../apis'
import styles from './index.module.less'
import './login.css'

const {
  Footer
} = Layout;

const FormItem = Form.Item

class Login extends React.Component {

  state = { loading: false }

  handleSubmit = e => {

    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const response = await api.post('/auth/login', values);
        if (response && response.status == 200) {
          const values = response.data.data;
          var CryptoJS = require("crypto-js");
          var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(values), 'yzhn user info');
          localStorage.setItem('usdt', ciphertext.toString());
          if (values.role === "1") {
            history.push('/')
            window.location.reload(false);
          } else {
            history.push('/')
            window.location.reload(false)
          }
        }
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { isloaded } = this.props
    return (
      <React.Fragment>
        <img alt="logo" src={logo} style={{ width: "10%", height: "13%" }} />
        <div className={styles.form}>
          <QueueAnim delay={200} type="top">
            <div className={styles.logo} key="1">

            </div>
            <div className={styles.textdes} key="2">
              <p style={{ fontSize: "30px", color: "#395D42", fontWeight: "70px" }}>Login</p>
            </div>
          </QueueAnim>
          <br />
          <form layout="vertical" onSubmit={this.handleSubmit}>
            <QueueAnim delay={200} type="top">
              <FormItem key="1">
                {getFieldDecorator('email', {
                  rules: [
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Required username',
                    },
                  ],
                })(<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="User Name" />)}
              </FormItem>
              <FormItem key="2">
                {getFieldDecorator('password', {
                  rules: [
                    {
                      required: true,
                      message: 'Required password',
                    },
                  ],
                })(<Input.Password prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />)}
              </FormItem>
              <FormItem key="3">
                <Button style={{ backgroundColor: "#3BCB4D", color: "white", borderRadius: "10px" }} htmlType="submit" size="default" loading={isloaded}>
                  SIGN IN
                  </Button>
              </FormItem>
            </QueueAnim>
          </form>
        </div>
        {/* <div className={styles.footer}>
          <Footer style={{ textAlign: 'center', background: '#fff' }}>
            {config.footerText}
          </Footer>
        </div> */}
      </React.Fragment>
    )
  }
}
function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded
  }
}
export default connect(mapStateToProps, { signIn, currentUser })(Form.create()(Login));




