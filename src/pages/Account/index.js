import React from 'react';
import { Table, Popconfirm, Icon, Button, Col, Row, Input, Select, Breadcrumb } from 'antd';
import history from '../../router/history'
import PageHeaderWrapper from '../../components/PageHeaderWrapper'
//component
import EditableTable from '../../components/User/CustomTable';

import { getUserInfo, getUserToken } from '../../utils';
import { fetchAccount, putAccount, postAccount, deleteAccount } from '../../actions/Account';
import { connect } from "react-redux";
const uuidv4 = require('uuid/v4');


class Account extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    this.getAllAccount();
  }

  getAllAccount() {
    this.props.fetchAccount()
  }

  //update Account
  editAccount = (data, id) => {
    this.props.putAccount(data, id);
  }

  // to create new Account
  createNewAccount = (data) => {
    let userInfo = getUserInfo();
    data.created_by = "admin";
    data.updated_by = '';
    this.props.postAccount(data);
  }

  // to delete Account
  deleteAccount = (id) => {
    this.props.deleteAccount(id);
  }

  btnCreate = () => {
    history.push('/account/create')
  }

  render() {
    var columns = [
      {
        title: 'Name',
        width: 120,
        dataIndex: 'name',
        key: 'name',
        fixed: 'left',
        align: 'center',
        sorter: (a, b) => a.name.length - b.name.length,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Employee_Code',
        dataIndex: 'ecode',
        key: '1',
        align: 'center',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.ecode - b.ecode,
      },
      {
        title: 'NIRC',
        dataIndex: 'nirc',
        key: '2',
        align: 'center',
        sorter: (a, b) => a.nirc.length - b.nirc.length,
        sortDirections: ['descend', 'ascend'],
      },

      {
        title: 'Phone_Number',
        dataIndex: 'phone_number',
        key: '3',
        align: 'center',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.phone_number - b.phone_number,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: '4',
        align: 'center',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.email - b.email,
      },
    ];
    const perform = {
      create: "account:create",
      edit: "account:edit"
    }
    const newData = {
      title: "",
      description: ""
    }

    let data = this.props.account;
    data.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
      console.log("KEY: " + d.key);
    })

    return (
      <div >
        <PageHeaderWrapper />

        <h3 style={{ marginBottom: '10px', marginTop: '20px' }}>Account Detail</h3>
        <p>You can add  new account  data by entering  one after clicking the add new button and can see the Employee data in table.</p>

        <Row style={{ marginBottom: '30px', marginTop: '30px' }}>
          <Button type="primary" style={{ backgroundColor: '#082C13', height: '50px' }} onClick={this.btnCreate} >Create New Account</Button>
        </Row>


        <EditableTable
          dataSource={data}
          columns={columns}
          role="Admin"
          title="Account List"
          size="middle"
          perform={perform}
          newData={newData}
          editData={(data, id) => this.editAccount(data, id)}
          createNewData={(data) => this.createNewAccount(data)}
          deleteData={(id) => this.deleteAccount(id)}
        />

      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    account: state.account.list,
  };
}
export default connect(
  mapStateToProps,
  { fetchAccount, putAccount, postAccount, deleteAccount }
)(Account);
