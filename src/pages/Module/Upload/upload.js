import React from 'react'
import { Form, Input, Col, Row, Divider, Icon, Select, Button, Breadcrumb } from 'antd'
import { fetchModule, putModule, postModule, deleteModule } from '../../../actions/Module'
import api from 'apis'
import { noti } from 'utils/index'
import { connect } from "react-redux"
import history from '../../../router/history'
import PageHeaderWrapper from '../../../components/PageHeaderWrapper';

class CreateEmployee extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        mname: "",
        cname: "",
        aname: "",
        remark: "",
        created_by: "",
        updated_by: ""
      }
    }
  }



  handleChange = (e) => {
    this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } })
  }



  handleSubmit = e => {
    e.preventDefault();
    let { data } = this.state;
    data.created_by = "admin";
    data.updated_by = '';
    this.props.form.validateFieldsAndScroll((err, fdata) => {
      if (!err) {
        this.props.postModule(data);
        this.props.history.push('/module')
        const values = {
          ...fdata,

        }

        api.post('modules', values).then((result) => {
          if (result) {
            this.props.form = '';
          }
        })
        noti('success', 'Successfully!', 'Module has been created successfully.')
      } else {
        noti('error', 'Unsuccessfully!', 'Fail to Create.')
      }
    });
  };

  btnCancel = () => {
    history.push('/module/')
  }

  render() {
    const { TextArea } = Input;
    const { data } = this.state;




    return (
      <div >
        <PageHeaderWrapper />
        <div style={{ marginBottom: '25px' }}>
          <h3 style={{ marginBottom: '15px', marginTop: '15px' }}>Module Information</h3>
        </div>

        <Form onSubmit={this.handleSubmit}>
          <div style={{ marginLeft: '25px' }}>
            <Row>
              <Col span={12}>
                <Form.Item label="Module Name">
                  {


                    (<Input placeholder="Please Enter Module Name:" style={{ width: '80%' }} name="mname"
                      value={data.mname}
                      onChange={this.handleChange} />)}
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item label="Controller Name:">


                  {
                    (<Input placeholder="Please Enter Controller Name:" style={{ width: '80%' }} name="cname"
                      value={data.cname}
                      onChange={this.handleChange} />)}
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={12}>
                <Form.Item label="Action Name:">
                  {
                    (<Input placeholder="Please Enter Action Name" style={{ width: '80%' }} name="aname"
                      value={data.aname}
                      onChange={this.handleChange} />)}
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item label="Remark:">
                  {



                    (<TextArea row={2} placeholder="Please Enter Remark" style={{ width: '80%' }} name="remark"
                      value={data.remark}
                      onChange={this.handleChange} />)}
                </Form.Item>
              </Col>
            </Row>




            <Row>
              <Form.Item wrapperCol={{ span: 12 }}>
                <Button type="primary" htmlType="submit" style={{ backgroundColor: "green", color: "white", borderColor: 'green' }}>
                  Submit
                      </Button>
                <Button type="primary" onClick={this.btnCancel} style={{ backgroundColor: "white", color: "black", marginLeft: "15px", borderColor: 'gray' }}>
                  Cancel
                      </Button>
              </Form.Item>
            </Row>
          </div>
        </Form>
      </div>
    );
  }
}
const Module = Form.create({})(CreateEmployee)

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    module: state.module.list,

  };
}
export default connect(
  mapStateToProps,
  { fetchModule, putModule, postModule, deleteModule }
)(Module);