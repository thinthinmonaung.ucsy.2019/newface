import React from 'react';
import { Table, Popconfirm,Icon,Button,Col,Row ,Input,Select,Breadcrumb} from 'antd';
import history from '../../router/history'
//component
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import EditableTable from '../../components/Employee/CustomTable';
import { getUserInfo, getUserToken } from '../../utils';
import Can from '../../utils/Can';
import Forbidden from '../Forbidden';
import {fetchModule, putModule,postModule,deleteModule} from '../../actions/Module';
import { connect } from "react-redux";
const uuidv4 = require('uuid/v4');


class Module extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount(){   
    this.getAllModule();
  }

   getAllModule(){
    this.props.fetchModule()
  }

  //update Module
  editModule=(data,id)=>{
   this.props.putModule(data,id);
  }

  // to create new Module
  createNewModule=(data)=>{
    let userInfo=getUserInfo();
    data.created_by="admin";
    data.updated_by='';
    this.props.postModule(data);
  }

   // to delete Module
   deleteModule=(id)=>{
    this.props.deleteModule(id);
  }

  btnCreate=()=>{
    history.push('/module/create')
  }

  render() {
    var columns = [
        {
            title: 'Module Name',
            width: 200,
            dataIndex: 'mname',
            key:'mname',
            fixed:'left',
            align:'center',
            sorter: (a, b) => a.mname.length - b.mname.length,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Controller Name',
            dataIndex: 'cname',
            key:'cname',
            align:'center',
            defaultSortOrder: 'descend',
            sorter: (a, b) => a.cname - b.cname,
          },
          {
            title: 'Action Name',
            dataIndex: 'aname',
            key:'aname',
            align:'center',
            sorter: (a, b) => a.aname.length - b.aname.length,
            sortDirections: ['descend', 'ascend'],
          },
          {
            title: 'Remark',
            dataIndex: 'remark',
            key:'remark',
            align:'center',
          },
    ];
    const perform = {
      create: "module:create",
      edit: "module:edit"
    }
    const newData = {
      title: "",
      description: ""
    }

    let data=this.props.module;
      data.map(d=>{
        let uuid=uuidv4();
        d.key=uuid;
      })

    return (
        <div>
          <PageHeaderWrapper/>
       <Row style={{marginBottom:'30px',marginTop:'30px'}}>
          <Button type="primary" style={{backgroundColor:'#082C13',height:'50px'}} onClick={this.btnCreate} >Create New Module</Button>
        </Row>
         
          
          <EditableTable
            dataSource={data}
            columns={columns}
            role="Admin"
            title="Module List"
            size="middle"
            perform={perform}
            newData={newData}
            getData={this.getAllDepartment}
            editData={(data,id)=>this.editModule(data,id)}
            createNewData={(data)=>this.createNewModule(data)}
            deleteData={(id)=>this.deleteModule(id)}
          />
        
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    module: state.module.list,
  };
}
export default  connect(
  mapStateToProps,
  { fetchModule,putModule,postModule,deleteModule }
)(Module);