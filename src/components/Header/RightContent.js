import React from 'react'
import { connect } from 'react-redux'
import { Avatar, Icon, Spin, Menu, Dropdown, Row, Col, Button ,Divider} from 'antd'
import { FormattedMessage } from 'umi-plugin-locale';
import HeaderDropdown from '../HeaderDropdown'
import styles from './index.module.less'
import { signOut } from '../../actions/Auth'
import 'antd/dist/antd.css'
import { Link } from 'react-router-dom';
import history from '../../router/history'
import { getUserInfo } from '../../utils'

const RightContent = ({ username, signOut }) => {

  const user = getUserInfo();

  return (

    <div style={{ float: 'right', marginTop: '1.5%' }}>
<a><Avatar icon="user" style={{
        backgroundColor: '#395d42', fontSize: '200%', marginLeft: '15px', marginRight: '10px'
      }} onClick={() => { history.push('/profile'); }} />

      <span style={{ fontSize: 20, color: 'white', marginRight: 10 }}>
        {user ? user.name : "Admin"}
      </span></a>
      {/* <span style={{
        color: 'white', width: '0px', fontSize: '340%', height: '500%',
        margin: 'auto', position: 'relative', overflow: 'hidden', top: '300%'
      }}>
        <div style={{
          position: 'absolute', backgroundColor: 'white', width: '1px',
          height: '70%', marginTop: '23px'
        }} />
      </span> */}
       <Divider type="vertical" />
      <a>
        <Avatar icon="logout" style={{
          backgroundColor: '#395d42', fontSize: '200%', marginLeft: '15px', marginRight: 10
        }} onClick={() => { localStorage.clear(); history.push('/');window.location.reload(false); }} />
      </a>
    </div>

  )
}

function mapStateToProps(state) {
  return {
    username: state.auth.username
  }
}

export default connect(mapStateToProps, { signOut })(RightContent)